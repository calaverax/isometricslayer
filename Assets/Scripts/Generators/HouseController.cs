﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;


public enum ResourceState
{
    Cooldown,
    ReadyToGenerate,
    Generating,
    ReadyToCollect
}

public enum Minigame
{
    Sheeps,
    Slash
}

public class HouseController : MonoBehaviour
{
    private ResourceState _resourceState = ResourceState.Cooldown;

    public Minigame MinigameToLoad;
    public SpriteRenderer HouseSpriteRenderer;
    public Transform EntrancePoint;
    
    private ClickToMove _clickToMove;

    public Color32 CooldownColor, NormalColor, HighlightedColor;
    public GameObject AutomatedLabel;
    public Slider ResourcesSlider;

    public float HouseCooldown = 5.0f;
    public float HouseGeneratingTime = 10.0f;
    public int GeneratedResources = 100;
    public int ClaimCost = 50;

    private bool _automated = false;
    public bool ClaimingState { get; private set; }
    private MonsterController _assignedMonster;
    
    private void Start()
    {
        string automationKey = PlayerPrefs.GetString(gameObject.name);
        if (!String.IsNullOrEmpty(automationKey))
        {
            if (automationKey.Equals("Automated"))
            {
                _automated = true;
                if (AutomatedLabel != null)
                {AutomatedLabel.SetActive(true);}
            }
        }
        
        GameObject character = GameObject.FindWithTag("PlayerMonster");
        ClaimingState = false;

        if (ResourcesSlider != null)
        {
            ResourcesSlider.value = 0;
            ResourcesSlider.maxValue = HouseGeneratingTime;
        }
            

        if (character != null)
        {
            _clickToMove = character.GetComponent<ClickToMove>();
        }
        SetHouseOnCooldown();
    }

    private void SetHouseOnCooldown()
    {
        _resourceState = ResourceState.Cooldown;
        HandleHouseState();
        
        StartCoroutine(HouseCooldownRoutine());
    }

    IEnumerator HouseCooldownRoutine()
    {
        yield return new WaitForSeconds(HouseCooldown);

        if (_automated)
        {
            _resourceState = ResourceState.Generating;
            HandleHouseState();
        }
        else
        {
            _resourceState = ResourceState.ReadyToGenerate;
            HandleHouseState();    
        }
        
    }

    private void HandleHouseState()
    {
        if (_resourceState == ResourceState.Cooldown)
        {
            if (HouseSpriteRenderer != null)
            {
                HouseSpriteRenderer.color = CooldownColor;
            }
            if (ResourcesSlider != null)
            {
                ResourcesSlider.gameObject.SetActive(false);
                ResourcesSlider.value = 0;
            }
        }
        else if (_resourceState == ResourceState.ReadyToGenerate)
        {
            if (HouseSpriteRenderer != null)
            {
                HouseSpriteRenderer.color = NormalColor;
            }
        }
        else if (_resourceState == ResourceState.Generating)
        {
            if (ResourcesSlider != null)
            {
                ResourcesSlider.gameObject.SetActive(true);
            }
        }
        else if (_resourceState == ResourceState.ReadyToCollect)
        {
            GameController.Instance.ReadyToCollectHouses.Add(this);
        }
    }
    
    public void HouseClicked()
    {
        if (!ClaimingState)
        {
            // If State is ReadyToGenerate
            if (_resourceState == ResourceState.ReadyToGenerate)
            {
                if (_assignedMonster == null)
                {
                    if (GameController.Instance == null)
                    {
                        Debug.LogError("Game Controller Is NULL!!!");
                        return;
                    }
                    // Get Closest Monster and order it to come
                    GameController.Instance.GetNearestAvailableMonster(EntrancePoint.position, controller =>
                    {
                        if (controller == null)
                        {
                            Debug.Log("No Monster Available");
                            return;
                        }
                            
                        
                        _assignedMonster = controller;
                        _assignedMonster.Locked = true;
                        _assignedMonster.PatrolBehaviour.PauseBehaviour();
                        _assignedMonster.MovementBehaviour.MoveToHouse(EntrancePoint.position, OnDestinationReached);
                            
                        Debug.Log("Monster: " + _assignedMonster.gameObject.name + " Assigned to House: " + gameObject.name);
                        
                    });
                }
            }
        }
        else
        {
            GameController.Instance.ClaimingMonster.MovementBehaviour.MoveToHouse(EntrancePoint.position, OnDestinationReached);
        }
    }

    private void OnDestinationReached(bool reached)
    {
        if (!ClaimingState)
        {
            if (reached)
            {
                // - Change State to Generating
                _resourceState = ResourceState.Generating;
                HandleHouseState();
            }
            else
            {
                _assignedMonster.Locked = false;
                _assignedMonster.PatrolBehaviour.ResumeBehaviour();
                _assignedMonster = null;
            }
        }
        else
        {
            PlayerPrefs.SetString("ClaimingHouse",gameObject.name);
            PlayerPrefs.Save();

            string minigameToLoad = "minigame_1";
            
            if (MinigameToLoad == Minigame.Slash)
                minigameToLoad = "minigame_2_rework";
            
            SceneManager.LoadScene(minigameToLoad, LoadSceneMode.Single);    
        }
        
        //Debug.Log("Destination Reached!");
        
    }

    private void Update()
    {
        if (_resourceState == ResourceState.Generating)
        {
            if (ResourcesSlider != null)
            {
                if (ResourcesSlider.value < HouseGeneratingTime)
                {
                    ResourcesSlider.value += Time.deltaTime;
                }
                else
                {
                    _resourceState = ResourceState.ReadyToCollect;
                    
                    if (!_automated)
                    {
                        
                        _assignedMonster.PatrolBehaviour.ResumeBehaviour();
                        _assignedMonster.Locked = false;
                        _assignedMonster = null;
                    }
                    
                    HandleHouseState();
                }
            }
        }
    }

    public void OnHouseCollected()
    {
        SetHouseOnCooldown();
    }

    public void ToggleClaimingState(bool claiming)
    {
        ClaimingState = claiming;
        
        if (ClaimingState)
        {
            HouseSpriteRenderer.color = HighlightedColor;
        }
        else
        {
            if (_resourceState == ResourceState.Cooldown)
            {
                HouseSpriteRenderer.color = CooldownColor;
            }
            else
            {
                HouseSpriteRenderer.color = NormalColor;
            }
        }
    }
}
