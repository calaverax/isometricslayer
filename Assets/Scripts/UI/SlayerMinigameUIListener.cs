﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SlayerMinigameUIListener : MonoBehaviour
{
    public TMP_Text KillsAmount;

    public TMP_Text PlayerLives;

    public TMP_Text CountdownLabel;

    private void Start()
    {
        SlashMinigameController.OnPlayerKillsModifiedEvent += OnKillsAmountModified;
        SlashMinigameController.OnPlayerLivesModifiedEvent += OnPlayerLivesModified;
        SlashMinigameController.OnCountdownModifiedEvent += OnCountdownModified;
    }

    private void OnCountdownModified(float time)
    {
        if (CountdownLabel != null)
            CountdownLabel.text = "Time Left: " + time.ToString("0.00");
    }

    private void OnDestroy()
    {
        SlashMinigameController.OnPlayerKillsModifiedEvent -= OnKillsAmountModified;
        SlashMinigameController.OnPlayerLivesModifiedEvent -= OnPlayerLivesModified;
        SlashMinigameController.OnCountdownModifiedEvent -= OnCountdownModified;
    }

    private void OnKillsAmountModified(int killsAmount)
    {
        if (KillsAmount != null)
            KillsAmount.text = "X" + killsAmount.ToString();
    }

    private void OnPlayerLivesModified(int playerLives)
    {
        if (PlayerLives != null)
            PlayerLives.text = "X" + playerLives.ToString();
    }
    
}
