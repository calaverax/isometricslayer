﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class UIScoreListener : MonoBehaviour
{
    public TMP_Text ScoreLabel; 
    void Start()
    {
        GameController.OnSoulsModifiedEvent += OnSoulsModifiedEvent;        
    }

    private void OnSoulsModifiedEvent(int amount)
    {
        ScoreLabel.text = GameController.Instance.CollectedSouls.ToString();
    }

}
