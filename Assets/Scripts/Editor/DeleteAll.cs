﻿// Clear all the editor prefs keys.
//
// Warning: this will also remove editor preferences as the opened projects, etc.

using UnityEngine;
using UnityEditor;

public class DeleteAll : MonoBehaviour
{
    [MenuItem("EditorPrefs/Clear all Editor Preferences")]
    static void deleteAllExample()
    {
        
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}