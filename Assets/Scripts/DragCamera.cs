﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCamera : MonoBehaviour
{
    Vector3 lastDragPosition;

    void Update()
    {
        UpdateDrag();
    }

    void UpdateDrag()
    {
        if (Input.GetMouseButtonDown(0))
            lastDragPosition = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            var delta = lastDragPosition - Input.mousePosition;
            transform.Translate(delta * Time.deltaTime);
            lastDragPosition = Input.mousePosition;
        }
    }
}
