﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour
{
    public PatrolRandomWaypoints PatrolBehaviour;
    public ClickToMove MovementBehaviour;

    //private bool pausedMovement = false;

    public bool Locked = false;

    private bool aboutToClaim = false;
    private void OnValidate()
    {
        PatrolBehaviour = GetComponent<PatrolRandomWaypoints>();
        MovementBehaviour = GetComponent<ClickToMove>();
    }

    public void CleanClaimingState()
    {
        PatrolBehaviour.ResumeBehaviour();
        aboutToClaim = false;
    }
    
    private void OnMouseDown()
    {
        aboutToClaim = !aboutToClaim;

        if (aboutToClaim)
        {
            PatrolBehaviour.PauseBehaviour();
            //pausedMovement = true;
            GameController.Instance.HighlightClaimeableHouses(this);    
        }
        else
        {
            PatrolBehaviour.ResumeBehaviour();
            //pausedMovement = false;
            GameController.Instance.StopClaimingHousesState();
        }
/*        
        if (pausedMovement)
            PatrolBehaviour.ResumeBehaviour();
        else
            PatrolBehaviour.PauseBehaviour();
        
        pausedMovement = !pausedMovement;
        */
    }
}
