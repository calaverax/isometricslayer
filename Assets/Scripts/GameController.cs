﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GameController : MonoBehaviour
{
    #region Singleton

    private static GameController _instance;
    public static GameController Instance
    {
        get {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameController>();
             
                if (_instance == null)
                {
                    GameObject container = new GameObject("GameController");
                    _instance = container.AddComponent<GameController>();
                }
            }
     
            return _instance;
        }
    }

    #endregion // Singleton

    public static Action<int> OnSoulsModifiedEvent;
    public int CollectedSouls { get; private set; }
    public MonsterController ClaimingMonster { get; private set; }
    
    public List<MonsterController> SpawnedMonsters = new List<MonsterController>();
    public List<HouseController> SpawnedHouses = new List<HouseController>();

    public List<HouseController> ReadyToCollectHouses = new List<HouseController>();
    
    private float _lesserMagnitude = Single.MaxValue;
    private MonsterController _closestMonster;
    private int calculatedPaths = 0;
    

    public void GetNearestAvailableMonster(Vector2 toLocation, Action<MonsterController> callback)
    {
        if (SpawnedMonsters == null)
        {
            Debug.LogError("Spawned Monsters List is NULL");
            return;
        }
        foreach (MonsterController spawnedMonster in SpawnedMonsters)
        {
            if (spawnedMonster == null)
            {
                Debug.LogError("Spawned Monster in List is NULL");
                return;
            }
            if (spawnedMonster.MovementBehaviour == null)
            {
                Debug.LogError("Spawned Monster Movement Behaviour is NULL");
                return;
            }
            spawnedMonster.MovementBehaviour.CalculatePath(toLocation, pathMagnitude =>
            {
                CalculatedPath(spawnedMonster, pathMagnitude, callback);
                /*
                if (pathMagnitude < _lesserMagnitude)
                {
                    _lesserMagnitude = pathMagnitude;
                    _closestMonster = spawnedMonster;
                }
                */
            });
        }
    }

    private void CalculatedPath(MonsterController monster, float magnitude, Action<MonsterController> callback)
    {
        calculatedPaths++;

        if (!monster.Locked)
        {
            if (magnitude < _lesserMagnitude)
            {
                _lesserMagnitude = magnitude;
                _closestMonster = monster;
            }    
        }

        if (calculatedPaths >= SpawnedMonsters.Count)
        {
            callback?.Invoke(_closestMonster);

            _closestMonster = null;
            _lesserMagnitude = Single.MaxValue;
            calculatedPaths = 0;
        }
            
    }

    public void OnHouseCollected(HouseController house)
    {
        if (ReadyToCollectHouses.Contains(house))
        {
            ReadyToCollectHouses.Find(obj => obj.name == house.name).OnHouseCollected();
            ReadyToCollectHouses.Remove(house);
        }
    }

    public void ModifyCollectedSouls(int amount)
    {
        CollectedSouls += amount;
        OnSoulsModifiedEvent?.Invoke(amount);
    }

    
    public void HighlightClaimeableHouses(MonsterController claimingMonster)
    {
        ClaimingMonster = claimingMonster;
        bool canClaimAHouse = false;
        
        foreach (HouseController spawnedHouse in SpawnedHouses)
        {
            if (CollectedSouls > spawnedHouse.ClaimCost)
            {
                spawnedHouse.ToggleClaimingState(true);
                canClaimAHouse = true;
            }
        }

        if (!canClaimAHouse)
        {
            claimingMonster.CleanClaimingState();
        }
    }

    public void StopClaimingHousesState()
    {
        ClaimingMonster = null;
        
        foreach (HouseController spawnedHouse in SpawnedHouses)
        {
            if (spawnedHouse.ClaimingState)
            {
                spawnedHouse.ToggleClaimingState(false);
            }
        }
    }

    public void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}
