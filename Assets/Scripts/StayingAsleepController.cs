﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StayingAsleepController : MonoBehaviour
{
    // Events
    public static Action<NewSheepController> OnSheepReachedEffectTriggerEvent;
    public static Action<NewSheepController> OnSheepReachedJumpTriggerEvent;
    public static Action<NewSheepController> OnSheepCollidedFenceEvent;
    public static Action<NewSheepController> OnSheepReachedFloorAfterJumpEvent;
    public static Action<NewSheepController> OnSheepReachedScreenEndEvent;
    
    [Header("General Settings")]
    public GameObject SheepsPrefab;
    public Transform SheepsSpawnPosition;
    public GameObject ParabolaRoot;
    
    [Header("Sheeps Config")]
    public float SheepsInitialSpeed = 250;
    public float SheepsJumpForce = 0.5f;

    [Header("Minigame Config")] 
    public float InitialDelayBeforeStart = 1;
    public float PlayerJumpReactionTime = 0.5f;
    public int PlayerLives = 3;
    public int SheepsToWin = 10;
    public float SpeedIncreaseRatio = 0.1f;
    //public int SpeedIncreaseCount = 1;

    [Header("Special Game Effects")]
    public int StartTriggeringAferSheeps = 3;
    public float ChanceForEffectToHappen = 0.25f;
    public int StackEffectAfter = 5;
    public int InitialEffectsDuration = 1;
    public float EffectDecreaseRatio = 0.05f;

    private List<NewSheepController> SpawnedSheeps = new List<NewSheepController>();
    private int _totalSheepsSpawned = 0;
    private int _totalSheepsEnded = 0;
    private float _sheepsCurrentSpeed;

    private void Start()
    {
        OnSheepReachedEffectTriggerEvent += OnSheepReachedEffectTrigger;
        OnSheepReachedJumpTriggerEvent += OnSheepReachedJumpTrigger;
        OnSheepCollidedFenceEvent += OnSheepCollidedFence;
        OnSheepReachedFloorAfterJumpEvent += OnSheepReachedFloorAfterJump;
        OnSheepReachedScreenEndEvent += OnSheepReachedScreenEnd;
        _sheepsCurrentSpeed = SheepsInitialSpeed;
    
        Invoke("SpawnSheep", InitialDelayBeforeStart);
    }

    private void OnDestroy()
    {
        OnSheepReachedEffectTriggerEvent -= OnSheepReachedEffectTrigger;
        OnSheepReachedJumpTriggerEvent -= OnSheepReachedJumpTrigger;
        OnSheepCollidedFenceEvent -= OnSheepCollidedFence;
        OnSheepReachedFloorAfterJumpEvent -= OnSheepReachedFloorAfterJump;
        OnSheepReachedScreenEndEvent -= OnSheepReachedScreenEnd;
    }

    void SpawnSheep()
    {
        if (SheepsPrefab == null)
        {
            Debug.LogError("Sheep Prefab Not Set wittin unity inspector");
            return;
        }

        if (SheepsSpawnPosition == null)
        {
            Debug.LogError("Sheep Spawn Transform Not Set wittin unity inspector");
            return;
        }

        GameObject newSheep = Instantiate(SheepsPrefab, SheepsSpawnPosition);

        if (newSheep != null)
        {
            NewSheepController sheepContoller = newSheep.GetComponent<NewSheepController>();

            if (sheepContoller != null)
            {
                SpawnedSheeps.Add(sheepContoller);

                float sheepSpeed = SheepsInitialSpeed;
                sheepSpeed += (SheepsInitialSpeed * SpeedIncreaseRatio) * _totalSheepsSpawned;

             //   if (_totalSheepsSpawned % SpeedIncreaseCount == 0)
                //{
                    // Cada X ovejas aumentar velocidad en 0.1
                   // sheepSpeed += (SheepsInitialSpeed * SpeedIncreaseRatio) * (_totalSheepsSpawned / SpeedIncreaseCount);
                    //_sheepsCurrentSpeed = sheepSpeed;
                //}
                
                sheepContoller.Initialize(sheepSpeed, ParabolaRoot);
            }
        }

        _totalSheepsSpawned++;
    }

    #region Sheeps Events

    private void OnSheepReachedEffectTrigger(NewSheepController sheepContoller)
    {
        if (_totalSheepsSpawned > StartTriggeringAferSheeps)
        {
            //tiempoDelEfecto -= (tiempoDelEfectoInicial * SpeedIncreaseRatio) * (_totalSheepsSpawned / SpeedIncreaseCount);
            float effectsDuration = InitialEffectsDuration;
            effectsDuration -= (InitialEffectsDuration * EffectDecreaseRatio) * _totalSheepsSpawned;
            effectsDuration = Mathf.Clamp(effectsDuration, 0.1f, 100);
//            effectsDuration -= (InitialEffectsDuration * SpeedIncreaseRatio) * (_totalSheepsSpawned / SpeedIncreaseCount);
            //Debug.Log("TimeReduction Product: " + (InitialEffectsDuration * SpeedIncreaseRatio) * (_totalSheepsSpawned / SpeedIncreaseCount));
            if ((_totalSheepsSpawned >= (StartTriggeringAferSheeps + StackEffectAfter)) && (_totalSheepsSpawned < (StartTriggeringAferSheeps + (StackEffectAfter * 2)))) 
                sheepContoller.TriggerGameEffect(2,ChanceForEffectToHappen, effectsDuration);
            else if (_totalSheepsSpawned >= (StartTriggeringAferSheeps + (StackEffectAfter * 2)))
                sheepContoller.TriggerGameEffect(3,ChanceForEffectToHappen, effectsDuration);
            else if (_totalSheepsSpawned < StartTriggeringAferSheeps + StackEffectAfter)
                sheepContoller.TriggerGameEffect(1,ChanceForEffectToHappen, effectsDuration);
        }
    }

    private void OnSheepReachedJumpTrigger(NewSheepController sheepContoller)
    {
        
    }

    private void OnSheepCollidedFence(NewSheepController sheepContoller)
    {
        // Discount Player Lives
        PlayerLives--;

        if (PlayerLives <= 0)
        {
            string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");
            if (!String.IsNullOrEmpty(houseClaimed))
            {
                PlayerPrefs.DeleteKey("ClaimingHouse");
                PlayerPrefs.Save();
            }
            SceneManager.LoadScene("SampleScene2");
        }
        else
        {
            // Spawn a New Sheep
            Invoke("SpawnSheep", 0);    
        }
    }

    private void OnSheepReachedFloorAfterJump(NewSheepController sheepContoller)
    {
        
    }

    private void OnSheepReachedScreenEnd(NewSheepController sheepContoller)
    {
        if (!sheepContoller.IsExtraSpawn)
        {
            _totalSheepsEnded++;
            Destroy(sheepContoller.gameObject);
            
            if (_totalSheepsEnded >= SheepsToWin)
            {
                // Triggers Game End
                string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");

                if (!String.IsNullOrEmpty(houseClaimed))
                {
                    PlayerPrefs.SetString(houseClaimed, "Automated");
                    PlayerPrefs.Save();
                }
                SceneManager.LoadScene("SampleScene2");
            }
            else
            {
                Invoke("SpawnSheep", 0);    
            }
        }
    }

    #endregion
    
}
