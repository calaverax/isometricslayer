﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorMinionController : MonoBehaviour
{
    public ClickToMove pathfindBehaviour;
    public HouseController AssignedHouse { get; private set; }
    
    public int collectedSouls = 0;
    public int HousesToCollect = 0;
    
    public void SendToDestination(Vector2 housePosition, Action<bool, CollectorMinionController> reached)
    {
        pathfindBehaviour.MoveToHouse(housePosition, houseReached =>
        {
            reached?.Invoke(houseReached, this);
        });
    }

    public void AssignToHouse(HouseController house)
    {
        AssignedHouse = house;
    }
}
