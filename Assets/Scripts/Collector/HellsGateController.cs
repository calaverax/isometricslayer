﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HellsGateController : MonoBehaviour
{
    public Transform EntrancePoint;
    public GameObject MinionPrefab;
    public int HousesNumber = 5; //número de casas máximo que recorre el collector
    bool collectorExist = false; //para chequear si existe y no mandar repetidos
    
    
    private void OnMouseDown()
    {
        // Check if there is any house to be collected
        if (GameController.Instance.ReadyToCollectHouses.Count > 0)
        {
            //Check if there's one collector already instatiated
            if (!collectorExist)
            {
                GameObject spawnedMinion = Instantiate(MinionPrefab, EntrancePoint, true);
                collectorExist = true;

                CollectorMinionController minionController = spawnedMinion.GetComponent<CollectorMinionController>();
                minionController.HousesToCollect = HousesNumber;

                if (minionController != null)
                {
                    // Send it to House with a callback on reached
                    minionController.AssignToHouse(GameController.Instance.ReadyToCollectHouses[0]);
                    minionController.SendToDestination(minionController.AssignedHouse.EntrancePoint.position, OnHouseReached);
                }
            }

            //- Voy a necesitar en el GameController lista de casas que generaron recursos
            //- Avisar que collecte esa casa para mandarla a cooldown    
        }
        
        
        
    }

    private void OnHouseReached(bool reached, CollectorMinionController minion)
    {
        // Tell Game controller that the house has been collected
        GameController.Instance.OnHouseCollected(minion.AssignedHouse);
        minion.collectedSouls += minion.AssignedHouse.GeneratedResources; 
        
        // On Reached House, check if there is more to collect,
        if (GameController.Instance.ReadyToCollectHouses.Count > 0 && minion.HousesToCollect >= 1)
        {
            minion.HousesToCollect--;
            // If there is more, send it.
            minion.AssignToHouse(GameController.Instance.ReadyToCollectHouses[0]);
            minion.SendToDestination(minion.AssignedHouse.EntrancePoint.position, OnHouseReached);
        }
        else // Else send it back to entrance with Callback on Reached
        {
            minion.AssignToHouse(null);
            minion.SendToDestination(EntrancePoint.position, OnBaseReached);
        }
    }

    private void OnBaseReached(bool reached, CollectorMinionController minion)
    {
        GameController.Instance.ModifyCollectedSouls(minion.collectedSouls);

        // Delete Minion and account points.
        collectorExist = false;
        Destroy(minion.gameObject);
    }

}
