﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepContoller : MonoBehaviour
{
    //public float MoveSpeed = 250f;

    public bool isExtraSpawn = false;
    private bool _isGrounded = false;
    private Rigidbody2D _rigidbody;
    private bool _canJump = false;

    private float _moveSpeed;
    private float _jumpForce;

    private bool _initialized = false;

    public void Initialize(float MoveSpeed, float JumpForce)
    {
        _moveSpeed = MoveSpeed;
        _jumpForce = JumpForce;

        _initialized = true;
    }
    
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (!_initialized) return;
            
        if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0)) || Input.touchCount > 0)
            Jump();
        
        transform.position = new Vector3(transform.position.x + ((Time.deltaTime * _moveSpeed) * -1), transform.position.y, transform.position.z);
    }

    public void InvertMoveSpeed()
    {
        _moveSpeed = _moveSpeed * -1;
    }

    void Jump()
    {
        if ((_rigidbody != null) && (_isGrounded) && (_canJump))
        {
            _rigidbody.AddForce(new Vector2(0, _jumpForce), ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor")
        {
            _isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor")
        {
            _isGrounded = false;
        }
    }

    private bool triggerExtraSheep = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Trigger1"))
        {
            // Speed - Turn Arround
            _canJump = true;
            SheepMinigameController.OnSheepTriggeredEvent(other.gameObject.name, this);

            if (!triggerExtraSheep)
            {
                if (Random.value >= 0.5)
                {
                    triggerExtraSheep = true;
                    SheepMinigameController.OnSheepExtraSpawnEvent();
                }
            }
            
        }
        else if (other.gameObject.name.Equals("Trigger2"))
        {
            // Fence
            SheepMinigameController.OnSheepCollidedEvent(this);
            if (!triggerExtraSheep)
            {
                if (Random.value >= 0.5)
                {
                    triggerExtraSheep = true;
                    SheepMinigameController.OnSheepExtraSpawnEvent();
                }
            }
            Destroy(this.gameObject);
        }
        else if (other.gameObject.name.Equals("Trigger3"))
        {
            SheepMinigameController.OnSheepReachedEndEvent(this);
            Destroy(this.gameObject);
        }
    }
}
