﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashBlade : MonoBehaviour
{
	public GameObject bladeTrailPrefab;
	public float minCuttingVelocity = .001f;

	bool isCutting = false;
	private Vector2 fingerDownPosition;
	Vector2 previousPosition;

	GameObject currentBladeTrail;

	Rigidbody2D rb;
	Camera cam;
	CircleCollider2D circleCollider;

	void Start ()
	{
		cam = Camera.main;
		rb = GetComponent<Rigidbody2D>();
		circleCollider = GetComponent<CircleCollider2D>();
	}

	// Update is called once per frame
	void Update () {
		
#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			StartCutting();
		} else if (Input.GetMouseButtonUp(0))
		{
			StopCutting();
		}
#else
		
		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Began)
			{
				StartCutting();
			}
			if (touch.phase == TouchPhase.Moved)
			{
				fingerDownPosition = touch.position;
			}
			if (touch.phase == TouchPhase.Ended)
			{
				StopCutting();
			}
		}
#endif		
		if (isCutting)
		{
			UpdateCut();
		}

	}

	void UpdateCut ()
	{
		Vector2 newPosition = Vector2.zero;
#if UNITY_EDITOR
		newPosition = cam.ScreenToWorldPoint(Input.mousePosition);
#else
		newPosition = cam.ScreenToWorldPoint(fingerDownPosition);
#endif		
        rb.position = newPosition;

		float velocity = (newPosition - previousPosition).magnitude * Time.deltaTime;
		if (velocity > minCuttingVelocity)
		{
			circleCollider.enabled = true;
		} else
		{
			circleCollider.enabled = false;
		}

		previousPosition = newPosition;
	}

	void StartCutting ()
	{
		isCutting = true;
		currentBladeTrail = Instantiate(bladeTrailPrefab, transform);
		previousPosition = cam.ScreenToWorldPoint(Input.mousePosition);
		circleCollider.enabled = false;
	}

	void StopCutting ()
	{
		isCutting = false;
		//currentBladeTrail.transform.SetParent(null);
		//Destroy(currentBladeTrail, 2f);
		circleCollider.enabled = false;
	}
}
