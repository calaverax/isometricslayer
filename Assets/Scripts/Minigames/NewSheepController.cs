﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum GameEffects
{
    REVERSE,
    SLOWDOWN,
    VANISH
}

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(ParabolaController))]
[RequireComponent(typeof(Animator))]
public class NewSheepController : MonoBehaviour
{

    public float SheepVanishDurationRatio = 0.5f;
    public bool IsExtraSpawn { get; private set; }
    public float JumpDistance = 6;
    public float JumpHeight = 2;
    public float SlowDownRatio = 0.55f;
   // public float EffectsDuration = 1.0f;
    public Color VanishingColor;
    public SpriteRenderer SheepRenderer;
    private Animator _animator;
    private GameObject _parabolaRoot;
    [SerializeField] private ParabolaController _parabolaController;

    private float _moveSpeed;
    private bool _initialized = false;
    private bool _isGrounded;
    private bool _sheepDiying = false;
    private bool _effectsAlreadyTriggered = false;
    private int _fadeTriggerHash;
    private float _initialMoveSpeed;

    private List<GameEffects> ApplyableGameEffects = new List<GameEffects>{ GameEffects.VANISH, GameEffects.REVERSE, GameEffects.SLOWDOWN };
    private List<GameEffects> TriggeredGameEffects = new List<GameEffects>();
    private void Start()
    {
        _parabolaController = GetComponent<ParabolaController>();
        _animator = GetComponent<Animator>();

        _fadeTriggerHash = Animator.StringToHash("TriggerFade");
    }

    public void Initialize(float speed, GameObject parabolaRoot, bool isExtraSpawn = false)
    {
        _initialMoveSpeed = _moveSpeed = speed;
        _parabolaRoot = parabolaRoot;
        
        IsExtraSpawn = isExtraSpawn;

        if (_parabolaController == null)
        {
            Debug.LogError("Sheep Does not Gave a ParabolaController");
            return;
        }

        _parabolaController.Speed = speed;
        _parabolaController.ParabolaRoot = _parabolaRoot;
        
        _initialized = true;
    }


    private void Update()
    {
        if (!_initialized || _sheepDiying) return;


            
        if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0)) || Input.touchCount > 0)
            Jump();
        
        transform.position = new Vector3(transform.position.x + ((Time.deltaTime * _moveSpeed) * -1), transform.position.y, transform.position.z);
    }

    private void Jump()
    {
        if (_parabolaController != null && !_parabolaController.Animation)
        {
            float sheepStartingHeight = transform.position.z;
            
            // Describe first Parabola point as SheepPosition
            _parabolaRoot.transform.GetChild(0).transform.position = this.transform.position;
            
            // Describe second Parabola point as (X = JumpDistance / 2) (Y = JumpHeight)
            _parabolaRoot.transform.GetChild(1).transform.position = new Vector3(transform.position.x - (JumpDistance * 0.5f), transform.position.y + JumpHeight);
            
            // Describe Third Parabola Point as (X = JumpDistance) (Y = StartingHeight)
            _parabolaRoot.transform.GetChild(2).transform.position = new Vector3(transform.position.x - JumpDistance, sheepStartingHeight);
            
            _parabolaController.FollowParabola();
        }
    }
    
    // Called from within animation event.
    private void OnFadeEndedEvent()
    {
        Destroy(this.gameObject);
    }

    public void TriggerGameEffect(int effectsAmount, float triggerChance, float effectsDuration)
    {
        for (int i = 0; i < effectsAmount; i++)
        {
            if (ApplyableGameEffects.Count >= 1)
            {
                int selectedEffect = Random.Range(0, ApplyableGameEffects.Count);

                if (Random.value <= triggerChance)
                {
                    TriggeredGameEffects.Add(ApplyableGameEffects[selectedEffect]);
                    ApplyableGameEffects.RemoveAt(selectedEffect);
                }
            }
        }
        
        foreach (GameEffects effect in TriggeredGameEffects)
        {
            Debug.Log("TriggeredGameEffect: " + effect.ToString());
            ActivateGameEffect(effect);
        }

        _effectsAlreadyTriggered = true;
        
        Invoke("RevokeGameEffects", effectsDuration);
        
        if (SheepVanishDurationRatio < effectsDuration)
            Invoke("RevokeVanishEffect", effectsDuration * SheepVanishDurationRatio);
    }

    private void RevokeVanishEffect()
    {
        if (TriggeredGameEffects.Contains(GameEffects.VANISH))
        {
            DeactivateGameEffect(GameEffects.VANISH);
        }
    }

    private void RevokeGameEffects()
    {
        foreach (GameEffects effect in TriggeredGameEffects)
        {
            Debug.Log("RevokedGameEffect: " + effect.ToString());
            DeactivateGameEffect(effect);
        }
    }

    private void ActivateGameEffect(GameEffects effect)
    {
        if (effect == GameEffects.VANISH)
        {
            //SheepRenderer.color = VanishingColor;
            SheepRenderer.enabled = false;
        }

        if (effect == GameEffects.REVERSE)
        {
            _moveSpeed = _moveSpeed * -1;
        }

        if (effect == GameEffects.SLOWDOWN)
        {
            _moveSpeed = _moveSpeed * SlowDownRatio;
        }
    }

    private void DeactivateGameEffect(GameEffects effect)
    {
        if (effect == GameEffects.VANISH)
        {
            SheepRenderer.enabled = true;
        }

        if (effect == GameEffects.REVERSE)
        {
            _moveSpeed = _initialMoveSpeed;
        }

        if (effect == GameEffects.SLOWDOWN)
        {
            _moveSpeed = _initialMoveSpeed;
        }
    }
    
    #region Collissions And Triggers

    private void OnCollisionEnter2D(Collision2D other)
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_sheepDiying)
            return;
        
        if (other.gameObject.name.Equals("ScreenEndTrigger"))
        {
            Debug.Log("Collided Game End position");
            
            StayingAsleepController.OnSheepReachedScreenEndEvent(this);
        }
        
        else if (other.gameObject.name.Equals("FenceCollider"))
        {
            StayingAsleepController.OnSheepCollidedFenceEvent(this);
            _animator.SetTrigger(_fadeTriggerHash);
            _moveSpeed = 0;
            _sheepDiying = true;
            
            if (_parabolaController != null)
                _parabolaController.StopFollow();
            
        }
        else if (other.gameObject.name.Equals("GameEffectTrigger"))
        {
            if (!_effectsAlreadyTriggered)
                StayingAsleepController.OnSheepReachedEffectTriggerEvent(this);
            
        }
        
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        
    }

    #endregion
}
