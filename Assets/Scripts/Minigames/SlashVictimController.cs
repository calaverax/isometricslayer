﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SlashVictimController : MonoBehaviour
{
    public Image VictimImage;
    public SpriteRenderer VictimRenderer;
    public Color32 CorrectVictimColor;
    //public float MoveSpeed = 100.0f;
    public float MinMoveSpeed = 500;
    public float MaxMoveSpeed = 600;

    private Color32 _currentColor;
    private bool _isCorrectVictim = false;
    private float _moveSpeed = 0;
    
    private List<Color> NoVictimColors = new List<Color>{
        Color.blue,
        Color.cyan,
        Color.gray,
        Color.green,
        Color.magenta,
        Color.red,
        Color.white,
        Color.yellow
        };

    private Transform _destiny;
    public void Initialize(bool isCorrectVictim, Transform Destiny, bool isFlipped)
    {
        _destiny = Destiny;
        if (isFlipped)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        if (isCorrectVictim)
        {
            _isCorrectVictim = true;
            if (VictimImage != null)
                VictimImage.color = CorrectVictimColor;
            else
                VictimRenderer.color = CorrectVictimColor;
        }
        else
        {
            if (VictimImage != null)
                VictimImage.color = NoVictimColors[Random.Range(0, NoVictimColors.Count)];
            else
                VictimRenderer.color = NoVictimColors[Random.Range(0, NoVictimColors.Count)];
        }
        
        _moveSpeed = Random.Range(MinMoveSpeed, MaxMoveSpeed);
    }

    private void LateUpdate()
    {
        
        
        float step =  _moveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _destiny.position, step);

        if (Vector3.Distance(transform.position, _destiny.position) < 0.001f)
        {
            if (_isCorrectVictim)
                SlashMinigameController.OnVictimReachedEndEvent(this);
            Destroy(this.gameObject);
        }
    }

    public void OnVictimClicked()
    {
        if (_isCorrectVictim)
        {
            SlashMinigameController.OnVictimSlashedEvent(this);
            Destroy(this.gameObject);
        }
        else
        {
            SlashMinigameController.OnInnocenSlashedEvent(this);
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isCorrectVictim)
        {
            SlashMinigameController.OnVictimSlashedEvent(this);
            Destroy(this.gameObject);
        }
        else
        {
            SlashMinigameController.OnInnocenSlashedEvent(this);
            Destroy(this.gameObject);
        }
    }
}
