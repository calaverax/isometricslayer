﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public enum DoorsSides
{
    LEFT,
    RIGHT
}
public class SlashMinigameController : MonoBehaviour
{
    public static Action<int> OnPlayerLivesModifiedEvent;
    public static Action<int> OnPlayerKillsModifiedEvent;
    public static Action<float> OnCountdownModifiedEvent; 
    
    public static Action<SlashVictimController> OnVictimReachedEndEvent;
    public static Action<SlashVictimController> OnVictimSlashedEvent;
    public static Action<SlashVictimController> OnInnocenSlashedEvent;
    
    public List<Transform> LeftPositions = new List<Transform>();
    public List<Transform> RightPositions = new List<Transform>();

    public int PlayerLives = 3;
    public int VictimsToSlash = 5;
    public float Countdown = 60.0f;
    public float DelayForGameStart = 1.0f;
    //public float DelayBetweenSpawns = 1.5f;
    public float MinDelayBetweenSpawns = 0.4f;
    public float MaxDelayBetweenSpawns = 0.8f;
    public float ChanceToSpawnRealVictim = 0.3f;
    public GameObject VictimsPrefab;

    private bool _isRealVictimOnScreen = false;

    private int _currentPlayerLives;
    private int _currentVictimsSlayed;
    private bool _isGamePlaying;
    private void Start()
    {
        if (LeftPositions.Count > 0 && RightPositions.Count > 0)
        {
            Invoke("StartSpawns", Random.Range(MinDelayBetweenSpawns, MaxDelayBetweenSpawns));
            //InvokeRepeating("StartSpawns", DelayForGameStart, DelayBetweenSpawns);
        }
        else
        {
            Debug.LogError("No Spawn Positions Setted for Victims");
        }

        OnVictimReachedEndEvent += OnVictimReachedEnd;
        OnVictimSlashedEvent +=  OnVictimSlashed;
        OnInnocenSlashedEvent += OnInnocenSlashed;

        _currentPlayerLives = PlayerLives;
        _currentVictimsSlayed = VictimsToSlash;
        _isGamePlaying = true;
        
        OnPlayerLivesModifiedEvent?.Invoke(_currentPlayerLives);
        OnPlayerKillsModifiedEvent?.Invoke(_currentVictimsSlayed);
    }

    private void Update()
    {
        if (_isGamePlaying)
        {
            Countdown -= Time.deltaTime;
            OnCountdownModifiedEvent?.Invoke(Countdown);

            if (Countdown <= 0)
            {
                _isGamePlaying = false;
                
                // Triggers Game End
                string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");

                if (!String.IsNullOrEmpty(houseClaimed))
                {
                    PlayerPrefs.SetString(houseClaimed, "Automated");
                    PlayerPrefs.Save();
                }
                SceneManager.LoadScene("SampleScene2");
            }    
        }
    }

    private void OnDestroy()
    {
        OnVictimReachedEndEvent -= OnVictimReachedEnd;
        OnVictimSlashedEvent -=  OnVictimSlashed;
        OnInnocenSlashedEvent -= OnInnocenSlashed;
    }

    private void OnInnocenSlashed(SlashVictimController controller)
    {
        _currentPlayerLives--;
        
        OnPlayerLivesModifiedEvent?.Invoke(_currentPlayerLives);

        if (_currentPlayerLives <= 0)
        {
            // Lose
            string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");
            if (!String.IsNullOrEmpty(houseClaimed))
            {
                PlayerPrefs.DeleteKey("ClaimingHouse");
                PlayerPrefs.Save();
            }
            SceneManager.LoadScene("SampleScene2");
        }
    }

    private void OnVictimSlashed(SlashVictimController controller)
    {
        _isRealVictimOnScreen = false;

        _currentVictimsSlayed--;
        //VictimsToSlash--;
        
        OnPlayerKillsModifiedEvent?.Invoke(_currentVictimsSlayed);

        if (_currentVictimsSlayed <= 0)
        {
            // Triggers Game End
            string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");

            if (!String.IsNullOrEmpty(houseClaimed))
            {
                PlayerPrefs.SetString(houseClaimed, "Automated");
                PlayerPrefs.Save();
            }
            SceneManager.LoadScene("SampleScene2");
            
        }
        
    }

    private void OnVictimReachedEnd(SlashVictimController controller)
    {
        _isRealVictimOnScreen = false;
    }

    private void StartSpawns()
    {
        DoorsSides spawnSide = DoorsSides.LEFT;

        if (Random.value > 0.5f)
        {
            spawnSide = DoorsSides.RIGHT;
        }

        Transform victimOrigin = LeftPositions[0];
        Transform victimDestiny = RightPositions[0];

        if (spawnSide == DoorsSides.LEFT)
        {
            victimOrigin = LeftPositions[Random.Range(0, LeftPositions.Count)];
            victimDestiny = RightPositions[Random.Range(0, RightPositions.Count)];
        }
        else if (spawnSide == DoorsSides.RIGHT)
        {
            victimOrigin = RightPositions[Random.Range(0, RightPositions.Count)];
            victimDestiny = LeftPositions[Random.Range(0, LeftPositions.Count)];
        }

        if (victimOrigin != null && victimDestiny != null)
        {
            GameObject victim = Instantiate(VictimsPrefab, victimOrigin.position, Quaternion.identity, victimOrigin);
            victim.transform.localScale = new Vector3(1,1,1);
            
            if (victim != null)
            {
                SlashVictimController victimController = victim.GetComponent<SlashVictimController>();

                if (victimController != null)
                {
                    if (_isRealVictimOnScreen)
                    {
                        victimController.Initialize(false, victimDestiny, spawnSide == DoorsSides.LEFT);
                    }
                    else
                    {
                        _isRealVictimOnScreen = Random.value <= ChanceToSpawnRealVictim;
                        victimController.Initialize(_isRealVictimOnScreen, victimDestiny, spawnSide == DoorsSides.LEFT);    
                    }
                }
            }
        }
        
        Invoke("StartSpawns", Random.Range(MinDelayBetweenSpawns, MaxDelayBetweenSpawns));
    }
}
