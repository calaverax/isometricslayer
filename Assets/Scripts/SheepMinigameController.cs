﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = Unity.Mathematics.Random;

public class SheepMinigameController : MonoBehaviour
{
    public static Action OnSheepExtraSpawnEvent;
    public static Action<SheepContoller> OnSheepCollidedEvent;
    public static Action<SheepContoller> OnSheepReachedEndEvent;
    public static Action<string, SheepContoller> OnSheepTriggeredEvent;
    
    public Transform SpawnPosition;
    public GameObject Prefab;
    public int PlayerLives = 3;

    public int SheepsToWin = 10;
    public float SheepsInitialSpeed = 250f;
    public float SheepsJumpForce = 0.5f;
    
    public float SpawnDelay;
    public float SpeedIncrease;
    public int IncreaseSpeedAfterShipsCount;
    public float ChanceForEffectToHappen = 0.3f;
    public float TimeResudctionPercent = 0.1f;
    public float EffectDuration = 2.0f;
    public float ReverseEffectDuration = 2.0f;
    private bool _isPlaying = true;

    private int _sheepCount = 0;
    

    private int _totalSheepsSpawned = 0;
    private int _timesSpeedIncreased = 0;

    void Start()
    {
      //  StartCoroutine(SpawnSheep());
      Invoke("SpawnSheep", 1.0f);
        OnSheepCollidedEvent += OnSheepCollided;
        OnSheepReachedEndEvent += OnSheepReachedEnd;
        OnSheepTriggeredEvent += OnSheepTriggered;
        OnSheepExtraSpawnEvent += OnSheepExtraSpawn;
    }

    private void OnSheepExtraSpawn()
    {
        GameObject sheep = Instantiate(Prefab, SpawnPosition);

        if (sheep != null)
        {
            ConfigureSheep(sheep);

            SheepContoller controller = sheep.GetComponent<SheepContoller>();
            
            if (controller != null)
            {
                controller.isExtraSpawn = true;
                float sheepSpeed = SheepsInitialSpeed;

                if (_totalSheepsSpawned % IncreaseSpeedAfterShipsCount == 0)
                {
                    //_timesSpeedIncreased++;
                    //sheepSpeed += (_timesSpeedIncreased * SpeedIncrease) * SheepsInitialSpeed;
                    Time.timeScale += SpeedIncrease;
                }
                    
                controller.Initialize(sheepSpeed, SheepsJumpForce);                
            }
        }
    }

    private void OnDestroy()
    {
        OnSheepCollidedEvent -= OnSheepCollided;
        OnSheepReachedEndEvent -= OnSheepReachedEnd;
        OnSheepTriggeredEvent -= OnSheepTriggered;
        OnSheepExtraSpawnEvent -= OnSheepExtraSpawn;
    }

    private float lastModifiedTimeScale = 1;
    
    private void OnSheepTriggered(string trigger, SheepContoller sheep)
    {
        if (trigger.Equals("Trigger1"))
        {
            float result = UnityEngine.Random.value;

            if (result <= ChanceForEffectToHappen)
            {
                float game = UnityEngine.Random.value;
                lastModifiedTimeScale = Time.timeScale;

                // SlowDown
                if (game <= 0.5)
                {
                    Time.timeScale = Time.timeScale * TimeResudctionPercent;
                    Invoke("RestoreDeltaTime", EffectDuration);
                }
                else // Reverse
                {
                    sheep.InvertMoveSpeed();
                    StartCoroutine(InvertSheepMovement(sheep));
                }
            }
        }
    }

    private IEnumerator InvertSheepMovement(SheepContoller sheep)
    {
        yield return  new WaitForSeconds(ReverseEffectDuration);
            
        sheep.InvertMoveSpeed();
    }
    
    private void RestoreDeltaTime()
    {
        Time.timeScale = lastModifiedTimeScale;
    }

    private void OnSheepReachedEnd(SheepContoller sheep)
    {
        if (!sheep.isExtraSpawn)
            SpawnSheep();
        
        _sheepCount++;

         // We need to know from wich house the minigame was loaded, to tell that house has been taken
        if (_sheepCount == SheepsToWin)
        {

            string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");

            if (!String.IsNullOrEmpty(houseClaimed))
            {
                PlayerPrefs.SetString(houseClaimed, "Automated");
                PlayerPrefs.Save();
            }
        }

    }

    private void OnSheepCollided(SheepContoller sheep)
    {
        PlayerLives--;

        if (PlayerLives >= 0)
        {
            if (!sheep.isExtraSpawn)
                SpawnSheep();
        }
        else
        {
            Time.timeScale = 1;
            string houseClaimed = PlayerPrefs.GetString("ClaimingHouse");
            if (!String.IsNullOrEmpty(houseClaimed))
            {
                PlayerPrefs.DeleteKey("ClaimingHouse");
                PlayerPrefs.Save();
            }
            SceneManager.LoadScene("SampleScene");
        }
        //_sheepCount--;
        // StopCoroutine(SpawnSheep());
        // SceneManager.UnloadSceneAsync("StayingAsleepScene");
    }

    public void SpawnSheep()
    {
        GameObject sheep = Instantiate(Prefab, SpawnPosition);

        if (sheep != null)
        {
            ConfigureSheep(sheep);
          //  _sheepCount++;
            _totalSheepsSpawned++;
        }
    }

/*
    IEnumerator SpawnSheep()
    {
        while (_isPlaying)
        {
            yield return new WaitForSeconds(SpawnDelay);

            if (_sheepCount < MaxSheepsSpawned)
            {
                GameObject sheep = Instantiate(Prefab, SpawnPosition);

                if (sheep != null)
                {
                    ConfigureSheep(sheep);
                    _sheepCount++;
                    _totalSheepsSpawned++;
                }
            }
        }
    }
    */

    private void ConfigureSheep(GameObject obj)
    {
        SheepContoller controller = obj.GetComponent<SheepContoller>();

        if (controller != null)
        {
            float sheepSpeed = SheepsInitialSpeed;

            if (_totalSheepsSpawned % IncreaseSpeedAfterShipsCount == 0)
            {
                //_timesSpeedIncreased++;
                //sheepSpeed += (_timesSpeedIncreased * SpeedIncrease) * SheepsInitialSpeed;
                Time.timeScale += SpeedIncrease;
            }
                
            controller.Initialize(sheepSpeed, SheepsJumpForce);                
        }
    }

}
