using System;
using UnityEngine;
using System.Collections.Generic;
using PolyNav;

//example
[RequireComponent(typeof(PolyNavAgent))]
public class ClickToMove : MonoBehaviour
{

    private PolyNavAgent _agent;
    private PolyNavAgent agent {
        get { return _agent != null ? _agent : _agent = GetComponent<PolyNavAgent>(); }
    }

    private PolyNav2D _polyNav2D;
    private void Start()
    {

        _polyNav2D = GameObject.FindObjectOfType<PolyNav2D>();
        // PolyNav2D.FindPath
    }

    public void MoveToHouse(Vector2 destination, Action<bool> Callback)
    {
        //CalculatePath(destination);
        agent.SetDestination(destination, Callback);
    }
    
    /*
    void Update() {
        if (Input.GetMouseButtonDown(0))
        {
            CalculatePath();
        }
        if ( Input.GetMouseButton(0) ) {
            agent.SetDestination(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            Debug.Log("Destination Set To: " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
    
*/
    public void CalculatePath(Vector2 destination, Action<float> Magnitude)
    {
        _polyNav2D.FindPath(transform.position, destination, vector2s =>
        {
            if (vector2s == null)
            {
                Debug.Log("NULL RESULT VECTOR!!!");
                return;
            }
            float totalMagnitude = 0.0f;

            foreach (Vector2 vector2 in vector2s)
            {
                totalMagnitude += vector2.magnitude;
            }
            
            Magnitude?.Invoke(totalMagnitude);
            
            /*
            Debug.Log("Vector Total Magnitude: (Idividual Sum)" + totalMagnitude);
            
            Vector2 resultVector = Vector2.zero;

            foreach (Vector2 vector2 in vector2s)
            {
                resultVector += vector2;
            }
            
            Debug.Log("Vector Total Magnitude: (Sum of Vectors)" + resultVector.magnitude);
            */
        });
    }
}